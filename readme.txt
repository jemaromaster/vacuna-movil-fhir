Para que funcione la autenticación, modificar el archivo standalone.xml

agregar dentro del tag de security el siguiente security-domain:
y datasources
<<datasource jta="false" jndi-name="java:/Cancha" pool-name="Cancha" enabled="true" use-java-context="true" use-ccm="false">
                    <connection-url>jdbc:postgresql://localhost:5432/cancha</connection-url>
                    <driver-class>org.postgresql.Driver</driver-class>
                    <driver>postgresql</driver>
                    <pool>
                        <min-pool-size>2</min-pool-size>
                        <max-pool-size>20</max-pool-size>
                    </pool>
                    <security>
                        <user-name>postgres</user-name>
                        <password>postgres</password>
                    </security>
                    <validation>
                        <validate-on-match>false</validate-on-match>
                        <background-validation>false</background-validation>
                        <background-validation-millis>1</background-validation-millis>
                    </validation>
                    <statement>
                        <prepared-statement-cache-size>0</prepared-statement-cache-size>
                        <share-prepared-statements>false</share-prepared-statements>
                    </statement>
                </datasource>


 <security-domain name="cancha" cache-type="default">
                    <authentication>
                        <login-module code="Database" flag="required">
                            <module-option name="dsJndiName" value="java:/Cancha"/>
                            <module-option name="principalsQuery" value="select usuario0_.password from Usuario usuario0_ where usuario0_.username=?"/>
                            <module-option name="rolesQuery" value="select rol.descripcion, 'Roles' from Usuario, rol_usuario, rol where usuario.id = rol_usuario.id_usuario and rol_usuario.id_rol = rol.id and usuario.username = ?"/>
                            <module-option name="hashAlgorithm" value="MD5"/>
                            <module-option name="hashEncoding" value="hex"/>
                        </login-module>
                    </authentication>
                </security-domain>
