package cancha.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.ticpy.tekoporu.annotation.NextView;
import org.ticpy.tekoporu.annotation.PreviousView;
import org.ticpy.tekoporu.stereotype.ViewController;

import ca.uhn.fhir.model.dstu.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu.composite.CodingDt;
import ca.uhn.fhir.model.dstu.composite.QuantityDt;
import ca.uhn.fhir.model.dstu.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu.resource.Immunization;
import ca.uhn.fhir.model.dstu.resource.Immunization.Explanation;
import ca.uhn.fhir.model.dstu.resource.Patient;
import ca.uhn.fhir.model.primitive.DateDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import cancha.domain.Vacuna;
import ca.uhn.fhir.model.dstu.valueset.ImmunizationRouteCodesEnum;

@ViewController
public class MapperFHIRDomain {

	Immunization immunization;


	String serverBase;

	public Immunization map(Vacuna vac, Patient patient) {

		// se obtiene del web.xml la URI base
		FacesContext fc = FacesContext.getCurrentInstance();
		serverBase = fc.getExternalContext().getInitParameter(
				"metadata.serviceBaseURI");

		Immunization immunization = new Immunization();

		// identifier
		// immunization.addIdentifier().setValue(vac.getId() + "");

		// date
		if (vac.getFecha() != null) {
			DateTimeDt fecha = new DateTimeDt();
			fecha.setValueAsString(new SimpleDateFormat("YYYYMMdd")
					.format(vac.getFecha()));
			immunization.setDate(fecha);
		}

		// vaccineType
		/*
		 * Un tipoVacuna tiene como tipo de dato un concepto codificable Un
		 * concepto codificable se compone de un texto y una lista de codigos
		 * correspondientes a algún estándar a los que puede mapearse
		 */
		if (vac.getTipoVacuna() != null) {
			CodeableConceptDt tipoVacuna = new CodeableConceptDt();
			tipoVacuna.setText(vac.getTipoVacuna()); // Descripcion del
														// atributo
														// codificable
			// CodingDt codificacion = new CodingDt(); //Tipo de dato para definir
			// codigos
			// codificacion.setCode("codigo de algun estandar");//valor del codigo

			// ArrayList listaCodigo = new ArrayList ();

			// listaCodigo.add(codificacion);// el tipo de vacuna se puede mapear a
			// mas de un codigo, por lo tanto se mete en una lista
			// tipoVacuna.setCoding(listaCodigo);
			immunization.setVaccineType(tipoVacuna);// finalmente se setea el valor
													// del tipoVacuna
		}

		

		// subject
		ResourceReferenceDt referenciaPaciente = new ResourceReferenceDt();

		if (patient!=null && patient.getIdentifierFirstRep() != null) {
			referenciaPaciente.setReference(serverBase
					+ patient.getIdentifierFirstRep().toString());
		} else {
			referenciaPaciente.setReference(serverBase + "1234");
		}
		immunization.setSubject(referenciaPaciente);

		// refuseIndicator -Sin correspondencia-

		// reported
		immunization.setReported(true);// true if the administration was
										// reported rather than directly
										// administered segun fhir

		// performer
		// ResourceReferenceDt referenciaEspecialista = new
		// ResourceReferenceDt();
		// referenciaEspecialista
		// .setReference("URLToSystem/FHIR/Practitioner/ID_DE_MEDICO");
		// immunization.setPerformer(referenciaEspecialista);

		// requester
		// ResourceReferenceDt referenciaSolicitante = new
		// ResourceReferenceDt();
		// referenciaSolicitante
		// .setReference("URLToSystem/FHIR/Practitioner/ID_DE_MEDICO");
		// immunization.setPerformer(referenciaEspecialista);

		// manufacturer

		if (vac.getFabricante() != null) {
			ResourceReferenceDt referenciaFabricante = new ResourceReferenceDt();
			referenciaFabricante.setReference(serverBase + "/Manufacturer/"
					+ vac.getFabricante());
			immunization.setManufacturer(referenciaFabricante);
		}

		// location

		if (vac.getHospitalNosocomio() != null) {
			ResourceReferenceDt referenciaLocation = new ResourceReferenceDt();
			referenciaLocation.setReference(serverBase + "/Location/"
					+ vac.getHospitalNosocomio());
			immunization.setLocation(referenciaLocation);
		}

		// lotNumber

		if (vac.getLoteNumero() != null) {
			immunization.setLotNumber(vac.getLoteNumero());
		}

		// expirationDate

		if (vac.getFechaVencimiento() != null) {
			DateDt fechaExpiracion = new DateDt();
			fechaExpiracion.setValueAsString(new SimpleDateFormat("YYYYMMdd")
					.format(vac.getFechaVencimiento()));
			immunization.setExpirationDate(fechaExpiracion);
		}

		// site
		/*
		 * Un bodySite tiene como tipo de dato un concepto codificable Un
		 * concepto codificable se compone de un texto y una lista de codigos
		 * correspondientes a algún estándar a los que puede mapearse
		 */

		if (vac.getSiteBrazo() != null) {
			CodeableConceptDt sitioCuerpo = new CodeableConceptDt();
			sitioCuerpo.setText(vac.getSiteBrazo()); // Descripcion del
														// atributo
														// codificable
			CodingDt codificacionBody = new CodingDt(); // Tipo de dato para
														// definir
														// codigos
			codificacionBody.setCode(vac.getSiteBrazo());// valor del codigo
			ArrayList<CodingDt> listaCodigoBody = new ArrayList<CodingDt>();
			listaCodigoBody.add(codificacionBody);// el BodySite se puede mapear
													// a mas
													// de un codigo, por lo
													// tanto se
													// mete en una lista

			sitioCuerpo.setCoding(listaCodigoBody);
			immunization.setSite(sitioCuerpo); // finalmente se setea el valor
												// del
												// BodySite
		}

		if (vac.getCantidadML() != null) {
			// dose
			QuantityDt dosis = new QuantityDt();
			dosis.setValue(vac.getCantidadML());
			dosis.setUnits("ML");
			immunization.setDoseQuantity(dosis);
		}

		// explanation.reason -Sin correspondencia-

		if (vac.getBooleanRechazo()) {
			// explanation.refusalReason
			Explanation vacunaExplicacion = new Explanation();
			CodeableConceptDt rechazoRazones = new CodeableConceptDt();
			rechazoRazones.setText(vac.getObservacionRechazo());
			ArrayList<CodeableConceptDt> listaRechazo = new ArrayList<CodeableConceptDt>();
			listaRechazo.add(rechazoRazones);
			vacunaExplicacion.setRefusalReason(listaRechazo);
			immunization.setExplanation(vacunaExplicacion);
		}

		// reaction.date
		// Reaction reaccionVacuna = new Reaction();
		// DateTimeDt fechaReaccion = new DateTimeDt();
		// fechaReaccion.setValueAsString("19990203");
		// reaccionVacuna.setDate(fechaReaccion);

		// reaction.detail
		// ResourceReferenceDt referenciaRecurso = new ResourceReferenceDt();
		// referenciaRecurso.setReference(serverBase + "//id");// generico
		// porque
		// detail
		// en
		// FHIR
		// apunta
		// a
		// "any"
		// Resource
		// reaccionVacuna.setDetail(referenciaRecurso);

		if (vac.getViaRoute() != null) {
			immunization.setRoute(ImmunizationRouteCodesEnum.valueOf(vac
					.getViaRoute()));
		}
		// reaction.reported -Sin correspondencia-
		// vaccinationProtocol -Sin correspondencia-
		// doseSequence -Sin correspondencia-
		// description -Sin correspondencia-
		// authority -Sin correspondencia-
		// series -Sin correspondencia-
		// seriesDoses -Sin correspondencia-
		// doseTarget -Sin correspondencia-
		// doseStatus -Sin correspondencia-
		// doseStatusReason -Sin correspondencia-

		return immunization;
	}

}
