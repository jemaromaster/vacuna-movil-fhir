package cancha.util;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.ticpy.tekoporu.annotation.NextView;
import org.ticpy.tekoporu.annotation.PreviousView;
import org.ticpy.tekoporu.stereotype.ViewController;
import org.ticpy.tekoporu.template.AbstractListPageBean;

import ca.uhn.fhir.model.api.Bundle;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu.resource.Patient;
import cancha.domain.Usuario;

@ViewController
@NextView("/vacuna_list.xhtml")
@PreviousView("/index.xhtml")
public class PacienteUtils extends AbstractListPageBean<Usuario, Integer> {

	private static final long serialVersionUID = 1L;

	private String pacienteCedula;

	@Inject
	RestClient restClient;

	public String buscarPacientePorCedula() {

		Bundle bundle;
		Patient paciente;

		if (pacienteCedula != null && pacienteCedula.length() == 0) {
			FacesContext.getCurrentInstance().addMessage(
					"growl",
					new FacesMessage("Error",
							"Introduzca número de cédula del paciente"));
			return getPreviousView();
		}
//		bundle = restClient.getBundlePacientes(pacienteCedula);
		Patient p = null;
		p =  restClient.getBundlePacientes(pacienteCedula);
		if (p != null) {
//			List<IResource> listaPacientes = bundle.toListOfResources();
//			p = (Patient) listaPacientes.get(0);
//			System.out.println(p.getName().get(0).getNameAsSingleString()
//					+ p.getName().get(0).getGivenAsSingleString());
			this.guardarEnSesion("paciente", p);
			this.guardarEnSesion("pacienteCedula", pacienteCedula);
			return "/vacuna_list.xhtml";
		} else {
			FacesContext.getCurrentInstance().addMessage(
					"growl",
					new FacesMessage("Error",
							"No existe paciente asociado a la cédula "
									+ pacienteCedula));
			return getPreviousView();
		}

	}

	private void guardarEnSesion(String s, Object o) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.put(s, o);
	}

	@Override
	protected List<Usuario> handleResultList() {
		return null;
	}

	@PostConstruct
	void init() {

	}

	public String getPacienteCedula() {
		return pacienteCedula;
	}

	public void setPacienteCedula(String pacienteCedula) {
		this.pacienteCedula = pacienteCedula;
	}

}
