package cancha.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import cancha.domain.Usuario;


@FacesConverter("usuarioConverter")
public class UsuarioConverter implements Converter {
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		
		//Logger logger = LoggerFactory.getLogger(UsuarioConverter.class);		
		System.out.println("En UsuarioConverter.getAsObject " + arg2);
		Usuario usuario = null;
		try {
			System.out.println("Creando objeto");
			usuario = new Usuario();
			usuario.setId(Integer.parseInt(arg2));
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Retornando de UsuarioConverter.getAsObject");
		return usuario;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		
		//Logger logger = LoggerFactory.getLogger(UsuarioConverter.class);
		System.out.println("En UsuarioConverter.getAsString " + arg2);
		String usuario = "";
		if(arg2 != null && arg2 instanceof Usuario){
			System.out.println("Convirtiendo a string");
			usuario = ((Usuario)arg2).getId() + "";
		}
		System.out.println("Retornando UsuarioConverter.getAsString");
		return usuario;
	}	
}
