package cancha.util;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.ticpy.tekoporu.stereotype.ViewController;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.Bundle;
import ca.uhn.fhir.model.dstu.resource.Immunization;
import ca.uhn.fhir.model.dstu.resource.Patient;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.IGenericClient;

@ViewController
public class RestClient {

	Bundle retornoBundle;
	FhirContext ctx;
	private String serverBase = "http://localhost:8080/2LIRs/fhir/dstu1/";

	IGenericClient client;

	@PostConstruct
	protected void initialize() {
		ctx = FhirContext.forDstu1();

		// se obtiene del web.xml la URI base
		FacesContext fc = FacesContext.getCurrentInstance();
		serverBase = fc.getExternalContext().getInitParameter(
				"metadata.serviceBaseURI");

		client = ctx.newRestfulGenericClient(serverBase);
	}

	public String getServerBase() {
		return serverBase;
	}

	public void setServerBase(String serverBase) {
		this.serverBase = serverBase;
	}

	public Bundle getBundleVacunas(String idPaciente) {
		// ctx.newRestfulGenericClient(serverBase);
		retornoBundle = client
				.search()
				.forResource(Immunization.class)
				.where(Immunization.SUBJECT
						.hasChainedProperty(Patient.IDENTIFIER.exactly()
								.identifier(idPaciente))).execute();

		return retornoBundle;

	}

	public Patient getBundlePacientes(String id) {
		// ctx.newRestfulGenericClient(serverBase);
		// ca.uhn.fhir.model.api.Bundle results = client.search()
		// .forResource(Patient.class)
		// .where(Patient.IDENTIFIER.exactly().identifier(id))
		// .execute();

		// ca.uhn.fhir.model.api.Bundle results = client
		// .search()
		// .forResource(Patient.class)
		// .withIdAndCompartment(id, null)
		// .execute();

		Patient pat = client.read().resource(Patient.class).withId(id)
				.execute();
		return pat;
	}

	public void postVacuna(Immunization immu) {
		// Invoke the server create method (and send pretty-printed JSON
		// encoding to the server
		// instead of the default which is non-pretty printed XML)
		MethodOutcome outcome = client.create()
		   .resource(immu)
		   .prettyPrint()
		   .encodedJson()
		   .execute();
		 
		// The MethodOutcome object will contain information about the
		// response from the server, including the ID of the created
		// resource, the OperationOutcome response, etc. (assuming that
		// any of these things were provided by the server! They may not
		// always be)
		IdDt id = (IdDt) outcome.getId();
//		System.out.println("Se creo el recuro Immunization con ID: " + id.getValue());
	}

}
