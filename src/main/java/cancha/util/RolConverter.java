package cancha.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import cancha.domain.Rol;


@FacesConverter("rolConverter")
public class RolConverter implements Converter {
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		
		//Logger logger = LoggerFactory.getLogger(RolConverter.class);		
		System.out.println("En RolConverter.getAsObject " + arg2);
		Rol rol = null;
		try {
			System.out.println("Creando objeto");
			rol = new Rol();
			rol.setId(Integer.parseInt(arg2));
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Retornando de RolConverter.getAsObject");
		return rol;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		
		//Logger logger = LoggerFactory.getLogger(RolConverter.class);
		System.out.println("En RolConverter.getAsString " + arg2);
		String rol = "";
		if(arg2 != null && arg2 instanceof Rol){
			System.out.println("Convirtiendo a string");
			rol = ((Rol)arg2).getId() + "";
		}
		System.out.println("Retornando RolConverter.getAsString");
		return rol;
	}	
}
