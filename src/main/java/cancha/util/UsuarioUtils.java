package cancha.util;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.ticpy.tekoporu.stereotype.ViewController;

import cancha.domain.RolUsuario;
import cancha.domain.Usuario;

@ViewController
public class UsuarioUtils {

	private List<Usuario> usuarios;

	private String nombreEmpresa = "VacunaMe";

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public void guardarEnSesion(String s, Object o) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.put(s, o);
	}

	public Object obtenerDeSesion(String s) {
		Object obj = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap().get(s);
		
		return obj;
	}

	@PostConstruct
	void init() {

	}

	public String getUsername() {
		return FacesContext.getCurrentInstance().getExternalContext()
				.getUserPrincipal().getName();
	}

	public Usuario getUsuarioLogueado() {
		String username = FacesContext.getCurrentInstance()
				.getExternalContext().getUserPrincipal().getName();
		return new Usuario();
	}

	public boolean tieneRol(String rol) {

		Usuario usuario = getUsuarioLogueado();
		for (RolUsuario ur : usuario.getRolUsuarios()) {

			if (ur.getRol().getDescripcion().equals(rol))
				return true;
		}

		return false;
	}

	public static Boolean estaEntre(String desde, String hasta, String medio) {
		if (compararHoras(desde, medio) == 1
				&& compararHoras(medio, hasta) == 1)
			return true;
		else
			return false;
	}

	public static int compararHoras(String desde, String hasta) {
		String[] sDesde = desde.split(":");
		String[] sHasta = hasta.split(":");

		Integer dHora = Integer.parseInt(sDesde[0]);
		Integer dMin = Integer.parseInt(sDesde[1]);

		Integer hHora = Integer.parseInt(sHasta[0]);
		Integer hMin = Integer.parseInt(sHasta[1]);

		if (dHora > hHora) {
			return -1;
		} else if (dHora < hHora) {
			return 1;
		} else {// iguales
			if (dMin > hMin) {
				return -1;
			} else if (dMin < hMin) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	public static BigDecimal restarHoras(String desde, String hasta) {
		java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm");
		Long diff = null;
		BigDecimal hora = null;
		try {
			java.util.Date date1 = df.parse(desde);
			java.util.Date date2 = df.parse(hasta);
			diff = date2.getTime() - date1.getTime();
			hora = new BigDecimal(diff);
			hora = hora.divide(new BigDecimal(3600000));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return hora;
	}

	public static BigDecimal calcularPrecio(String desde, String hasta,
			BigDecimal precio) {
		java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm");

		BigDecimal r = null;

		Long tim;
		try {
			tim = df.parse(hasta).getTime() - df.parse(desde).getTime();
			r = new BigDecimal(tim);
			r = r.divide(new BigDecimal(3600)).multiply(precio);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return r;

	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

}
