/*
 * TICPY Framework
 * Copyright (C) 2012 Plan Director TICs
 *
----------------------------------------------------------------------------
 * Originally developed by SERPRO
 * Demoiselle Framework
 * Copyright (C) 2010 SERPRO
 *
----------------------------------------------------------------------------
 * This file is part of TICPY Framework.
 *
 * TICPY Framework is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License version 3
 * along with this program; if not,  see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301, USA.
 *
----------------------------------------------------------------------------
 * Este archivo es parte del Framework TICPY.
 *
 * El TICPY Framework es software libre; Usted puede redistribuirlo y/o
 * modificarlo bajo los términos de la GNU Lesser General Public Licence versión 3
 * de la Free Software Foundation.
 *
 * Este programa es distribuido con la esperanza que sea de utilidad,
 * pero sin NINGUNA GARANTÍA; sin una garantía implícita de ADECUACION a cualquier
 * MERCADO o APLICACION EN PARTICULAR. vea la GNU General Public Licence
 * más detalles.
 *
 * Usted deberá haber recibido una copia de la GNU Lesser General Public Licence versión 3
 * "LICENCA.txt", junto con este programa; en caso que no, acceda a <http://www.gnu.org/licenses/>
 * o escriba a la Free Software Foundation (FSF) Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02111-1301, USA.
 */
package cancha.view;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.ticpy.tekoporu.annotation.PreviousView;
import org.ticpy.tekoporu.stereotype.ViewController;
import org.ticpy.tekoporu.template.AbstractEditPageBean;
import org.ticpy.tekoporu.transaction.Transactional;

import cancha.business.RolBC;
import cancha.business.RolUsuarioBC;
import cancha.business.UsuarioBC;
import cancha.domain.Rol;
import cancha.domain.RolUsuario;
import cancha.domain.Usuario;

@ViewController
@PreviousView("/admin/usuario_list.xhtml")
public class UsuarioEditMB extends AbstractEditPageBean<Usuario, Integer> {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioBC usuarioBC;

	@Inject
	private RolBC rolBC;
	
	@Inject
	private RolUsuarioBC rolUsuarioBC;
	
	private List<Rol> listaRols;
	
	private Rol rolSeleccionado;
	
	private boolean cambiarContraseña;
	
	@Override
	@Transactional
	public String delete() {
		this.usuarioBC.delete(getId());
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario eliminado exitosamente", null);  
        FacesContext.getCurrentInstance().addMessage("suceso", msg); 
		return getPreviousView();
	}

	@Override
	@Transactional
	public String insert() {
		getBean().setEstado("act");
		try {//Encrypta contraseña
			String original = getBean().getPassword();
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(original.getBytes());
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}

			getBean().setPassword(sb.toString());
		} catch (NoSuchAlgorithmException e) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al guardar", null);  
	        FacesContext.getCurrentInstance().addMessage("suceso", msg);
	        return getNextView();
		}
		try {
			this.usuarioBC.insert(getBean());
			

			if(this.rolSeleccionado != null){
				this.rolSeleccionado = rolBC.load(this.rolSeleccionado.getId());
				RolUsuario rolUsuario = new RolUsuario();
				rolUsuario.setUsuario(getBean());
				rolUsuario.setRol(rolSeleccionado);
				
				if( getBean().getRolUsuarios() != null)
				for(RolUsuario rolU: getBean().getRolUsuarios()){
					rolUsuarioBC.delete(rolU.getId());
				}
				try{
					rolUsuarioBC.insert(rolUsuario);
				}catch(javax.persistence.PersistenceException ex){//Si existe el el rol
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "El rol ya esta asignado", null);  
			        FacesContext.getCurrentInstance().addMessage("suceso", msg);
			        return getPreviousView();
				}
			}
			
		}catch (Exception ex){
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al guardar", null);  
	        FacesContext.getCurrentInstance().addMessage("suceso", msg);
	        return getPreviousView();
		}
		
		
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario agregado exitosamente", null);  
        FacesContext.getCurrentInstance().addMessage("suceso", msg); 
		return getPreviousView();
	}

	@Override
	@Transactional
	public String update() {
		
		if(cambiarContraseña){
		
			try {//Encrypta contraseña
				String original = getBean().getPassword();
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(original.getBytes());
				byte[] digest = md.digest();
				StringBuffer sb = new StringBuffer();
				for (byte b : digest) {
					sb.append(String.format("%02x", b & 0xff));
				}
	
				getBean().setPassword(sb.toString());
			} catch (NoSuchAlgorithmException e) {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pudo guardar la clave", null);  
		        FacesContext.getCurrentInstance().addMessage("suceso", msg);
		        return getNextView();
			}
		}
		this.usuarioBC.update(getBean());
		
		if(this.rolSeleccionado != null){
			this.rolSeleccionado = rolBC.load(this.rolSeleccionado.getId());
			
			if( getBean().getRolUsuarios() != null && !getBean().getRolUsuarios().isEmpty()){
				if(getBean().getRolUsuarios().get(0).getRol().equals(rolSeleccionado)){
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario actualizado exitosamente", null);  
			        FacesContext.getCurrentInstance().addMessage("suceso", msg);
			        return getPreviousView();
				}
			}
			for(RolUsuario rolU: getBean().getRolUsuarios()){
				rolUsuarioBC.delete(rolU.getId());
			}
			RolUsuario rolUsuario = new RolUsuario();
			rolUsuario.setUsuario(getBean());
			rolUsuario.setRol(rolSeleccionado);
			try{
				rolUsuarioBC.insert(rolUsuario);
			}catch(javax.persistence.PersistenceException ex){//Si existe el el rol
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "El rol ya esta asignado", null);  
		        FacesContext.getCurrentInstance().addMessage("suceso", msg);
		        return getPreviousView();
			}
		}
		
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario actualizado exitosamente", null);  
        FacesContext.getCurrentInstance().addMessage("suceso", msg); 
		return getPreviousView();
	}

	@PostConstruct
	public void init() {
		setListaRols(rolBC.findAll());
		if(getBean().getRolUsuarios() != null && !getBean().getRolUsuarios().isEmpty()){
			this.rolSeleccionado = getBean().getRolUsuarios().get(0).getRol();
		}
	}
	
	@Override
	protected void handleLoad() {
		setBean(this.usuarioBC.load(getId()));
	}

	public List<Rol> getListaRols() {
		return listaRols;
	}

	public void setListaRols(List<Rol> listaRols) {
		this.listaRols = listaRols;
	}

	public Rol getRolSeleccionado() {
		return rolSeleccionado;
	}

	public void setRolSeleccionado(Rol rolSeleccionado) {
		this.rolSeleccionado = rolSeleccionado;
	}

	public boolean isCambiarContraseña() {
		return cambiarContraseña;
	}

	public void setCambiarContraseña(boolean cambiarContraseña) {
		this.cambiarContraseña = cambiarContraseña;
	}

	
}
