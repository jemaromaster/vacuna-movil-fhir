/*
 * TICPY Framework
 * Copyright (C) 2012 Plan Director TICs
 *
----------------------------------------------------------------------------
 * Originally developed by SERPRO
 * Demoiselle Framework
 * Copyright (C) 2010 SERPRO
 *
----------------------------------------------------------------------------
 * This file is part of TICPY Framework.
 *
 * TICPY Framework is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License version 3
 * along with this program; if not,  see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301, USA.
 *
----------------------------------------------------------------------------
 * Este archivo es parte del Framework TICPY.
 *
 * El TICPY Framework es software libre; Usted puede redistribuirlo y/o
 * modificarlo bajo los términos de la GNU Lesser General Public Licence versión 3
 * de la Free Software Foundation.
 *
 * Este programa es distribuido con la esperanza que sea de utilidad,
 * pero sin NINGUNA GARANTÍA; sin una garantía implícita de ADECUACION a cualquier
 * MERCADO o APLICACION EN PARTICULAR. vea la GNU General Public Licence
 * más detalles.
 *
 * Usted deberá haber recibido una copia de la GNU Lesser General Public Licence versión 3
 * "LICENCA.txt", junto con este programa; en caso que no, acceda a <http://www.gnu.org/licenses/>
 * o escriba a la Free Software Foundation (FSF) Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02111-1301, USA.
 */
package cancha.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.ticpy.tekoporu.annotation.NextView;
import org.ticpy.tekoporu.annotation.PreviousView;
import org.ticpy.tekoporu.stereotype.ViewController;
import org.ticpy.tekoporu.template.AbstractListPageBean;
import org.ticpy.tekoporu.transaction.Transactional;

import cancha.business.RolUsuarioBC;
import cancha.business.UsuarioBC;
import cancha.domain.RolUsuario;
import cancha.domain.Usuario;

@ViewController
@NextView("/admin/usuario_edit.xhtml")
@PreviousView("/admin/usuario_list.xhtml")
public class UsuarioListMB extends AbstractListPageBean<Usuario, Integer> {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UsuarioBC bc;
	@Inject
	private RolUsuarioBC rolUsuarioBC;
	
	private LazyDataModel<Usuario> model;	
	private int pageSize = 15; //default page size
		
	private Usuario usuario;

	@SuppressWarnings("serial")
	@PostConstruct
	public void loadLazyModel() {
		
		model = new LazyDataModel<Usuario>() {
			@Override
			public List<Usuario> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
					//SortOrder sort, Map<String, Object> filters) {
					//boolean sortOrder = false;
					//if(sort == SortOrder.DESCENDING)
					//	sortOrder = true;
				
					if(sortField == null) sortField = "id"; //default sort field
					
					List<Usuario> usuario = new ArrayList<Usuario>();
					usuario = bc.findPage(pageSize, first, sortField, (sortOrder.equals(SortOrder.ASCENDING)? true: false));
					
					//sort in ram
					//if (sortField != null) {
					//	Collections.sort(usuario,new LazySorter<Usuario>(sortField,sortOrder));
					//}
					
					return usuario;
			}

		};
		
		model.setRowCount(bc.count());
		model.setPageSize(pageSize);
		
	}
	
	@Override
	protected List<Usuario> handleResultList() {
		return this.bc.findAll();		
	}

	@Transactional
	public String deleteSelection() {
		boolean delete;
		for (Iterator<Integer> iter = getSelection().keySet().iterator(); iter.hasNext();) {
			Integer id = iter.next();
			delete = getSelection().get(id);

			if (delete) {
				bc.delete(id);
				iter.remove();
			}
		}
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Eliminado exitosamente", null);  
        FacesContext.getCurrentInstance().addMessage("suceso", msg); 
		return getPreviousView();
	}
	
	public LazyDataModel<Usuario> getModel() {
		return model;
	}

	public int getPageSize() {
		return pageSize;
	}

	@Transactional
	public String eliminar(){
		if(usuario != null && usuario.getId()!= null){
			usuario = bc.load(usuario.getId());
			if(usuario.getId()!=1){
				try{
					if(usuario.getRolUsuarios() != null){
						for(RolUsuario rolU: usuario.getRolUsuarios()){
							rolUsuarioBC.delete(rolU.getId());
						}
					}
					this.bc.delete(usuario.getId());
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Eliminado Exitosamente", null);  
			        FacesContext.getCurrentInstance().addMessage("suceso", msg);
			        return "#";
				}catch (Exception ex){
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "No puede eliminar al usuario, tiene operaciones realizadas", null);  
			        FacesContext.getCurrentInstance().addMessage("suceso", msg);
			        return "#";
				}
			}else{
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "No puede eliminar al administrador del sistema", null);  
		        FacesContext.getCurrentInstance().addMessage("suceso", msg); 
				return "#";
			}
		}
		return getPreviousView();
	}

	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}