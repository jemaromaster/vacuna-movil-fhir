/*
 * TICPY Framework
 * Copyright (C) 2012 Plan Director TICs
 *
----------------------------------------------------------------------------
 * Originally developed by SERPRO
 * Demoiselle Framework
 * Copyright (C) 2010 SERPRO
 *
----------------------------------------------------------------------------
 * This file is part of TICPY Framework.
 *
 * TICPY Framework is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License version 3
 * along with this program; if not,  see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301, USA.
 *
----------------------------------------------------------------------------
 * Este archivo es parte del Framework TICPY.
 *
 * El TICPY Framework es software libre; Usted puede redistribuirlo y/o
 * modificarlo bajo los términos de la GNU Lesser General Public Licence versión 3
 * de la Free Software Foundation.
 *
 * Este programa es distribuido con la esperanza que sea de utilidad,
 * pero sin NINGUNA GARANTÍA; sin una garantía implícita de ADECUACION a cualquier
 * MERCADO o APLICACION EN PARTICULAR. vea la GNU General Public Licence
 * más detalles.
 *
 * Usted deberá haber recibido una copia de la GNU Lesser General Public Licence versión 3
 * "LICENCA.txt", junto con este programa; en caso que no, acceda a <http://www.gnu.org/licenses/>
 * o escriba a la Free Software Foundation (FSF) Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02111-1301, USA.
 */
package cancha.view;

import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Inject;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.ticpy.tekoporu.annotation.NextView;
import org.ticpy.tekoporu.annotation.PreviousView;
import org.ticpy.tekoporu.stereotype.ViewController;
import org.ticpy.tekoporu.template.AbstractListPageBean;
import org.ticpy.tekoporu.transaction.Transactional;

import ca.uhn.fhir.model.api.Bundle;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu.resource.Immunization;
import ca.uhn.fhir.model.dstu.resource.Patient;
import cancha.domain.Usuario;
import cancha.util.RestClient;
import cancha.util.UsuarioUtils;

@ViewController
@NextView("/vacuna_edit.xhtml")
@PreviousView("/index.xhtml")
public class VacunaListMB extends AbstractListPageBean<Usuario, Integer> {

	private StreamedContent chart;

	private static final long serialVersionUID = 1L;

	private String nada = "+";
	@Inject
	RestClient restClient;

	@Inject
	UsuarioUtils usuarioUtils;

	private LazyDataModel<Usuario> model;
	private int pageSize = 15; // default page size

	private Usuario usuario;

	private Patient pacienteEnCuestion;
	private String pacienteCedula;

	private List<Immunization> listaVacunas;

	public StreamedContent getChart() {
		return chart;
	}

	public void setChart(StreamedContent chart) {
		this.chart = chart;
	}
	
//	public StreamedContent getStreamedImageById() {
//	    FacesContext context = FacesContext.getCurrentInstance();
//
//	    if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
//	        // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
//	        return new DefaultStreamedContent();
//	    }
//	    else {
//	        // So, browser is requesting the image. Get ID value from actual request param.
//	        String id = context.getExternalContext().getRequestParameterMap().get("id");
//	        Image image = service.find(Long.valueOf(id));
//	        return new DefaultStreamedContent(new ByteArrayInputStream(image.getBytes()));
//	    }
//	}

	@SuppressWarnings("serial")
	@PostConstruct
	public void loadLazyModel() {
		pacienteEnCuestion = (Patient) usuarioUtils.obtenerDeSesion("paciente");
		pacienteCedula = (String) usuarioUtils
				.obtenerDeSesion("pacienteCedula");
		
//		chart = new DefaultStreamedContent(new FileInputStream(chartFile), "image/png");
		this.findAllVacunas(pacienteCedula);
	}

	@Override
	protected List<Usuario> handleResultList() {
		// return this.bc.findAll();
		return null;
	}

	private void findAllVacunas(String identificadorPaciente) {

		Bundle bundle;
		Patient paciente;

		bundle = restClient.getBundleVacunas(identificadorPaciente); // cedula
		Immunization immunization = null;
		if (bundle.size() > 0) {
			this.listaVacunas = new ArrayList<Immunization>();
			List<IResource> listaImmunization = bundle.toListOfResources();
			for (IResource ires : listaImmunization) {
				immunization = (Immunization) ires;
				listaVacunas.add(immunization);
			}
		}

	}

	@Transactional
	public String deleteSelection() {
		// boolean delete;
		// for (Iterator<Integer> iter = getSelection().keySet().iterator();
		// iter.hasNext();) {
		// Integer id = iter.next();
		// delete = getSelection().get(id);
		//
		// if (delete) {
		// bc.delete(id);
		// iter.remove();
		// }
		// }
		// FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
		// "Eliminado exitosamente", null);
		// FacesContext.getCurrentInstance().addMessage("suceso", msg);
		return getPreviousView();
	}

	public String buscarOtroPaciente() {
		// FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
		// .remove("paciente");
		return "./index.xhtml?faces-redirect=true";
	}

	public String formatFecha(Date date) {
		if (date != null)
			return new SimpleDateFormat("dd/MM/yyyy").format(date);
		else
			return null;
	}

	public LazyDataModel<Usuario> getModel() {
		return model;
	}

	public int getPageSize() {
		return pageSize;
	}

	@Transactional
	public String eliminar() {
		return getPreviousView();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Patient getPacienteEnCuestion() {
		return pacienteEnCuestion;
	}

	public void setPacienteEnCuestion(Patient pacienteEnCuestion) {
		this.pacienteEnCuestion = pacienteEnCuestion;
	}

	public List<Immunization> getListaVacunas() {
		return listaVacunas;
	}

	public void setListaVacunas(List<Immunization> listaVacunas) {
		this.listaVacunas = listaVacunas;
	}

	public String getNada() {
		return nada;
	}

	public void setNada(String nada) {
		this.nada = nada;
	}
}