/*
 * TICPY Framework
 * Copyright (C) 2012 Plan Director TICs
 *
----------------------------------------------------------------------------
 * Originally developed by SERPRO
 * Demoiselle Framework
 * Copyright (C) 2010 SERPRO
 *
----------------------------------------------------------------------------
 * This file is part of TICPY Framework.
 *
 * TICPY Framework is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License version 3
 * along with this program; if not,  see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301, USA.
 *
----------------------------------------------------------------------------
 * Este archivo es parte del Framework TICPY.
 *
 * El TICPY Framework es software libre; Usted puede redistribuirlo y/o
 * modificarlo bajo los términos de la GNU Lesser General Public Licence versión 3
 * de la Free Software Foundation.
 *
 * Este programa es distribuido con la esperanza que sea de utilidad,
 * pero sin NINGUNA GARANTÍA; sin una garantía implícita de ADECUACION a cualquier
 * MERCADO o APLICACION EN PARTICULAR. vea la GNU General Public Licence
 * más detalles.
 *
 * Usted deberá haber recibido una copia de la GNU Lesser General Public Licence versión 3
 * "LICENCA.txt", junto con este programa; en caso que no, acceda a <http://www.gnu.org/licenses/>
 * o escriba a la Free Software Foundation (FSF) Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02111-1301, USA.
 */
package cancha.view;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.ticpy.tekoporu.annotation.PreviousView;
import org.ticpy.tekoporu.stereotype.ViewController;
import org.ticpy.tekoporu.template.AbstractEditPageBean;
import org.ticpy.tekoporu.transaction.Transactional;

import cancha.business.RolBC;
import cancha.business.RolUsuarioBC;
import cancha.business.UsuarioBC;
import cancha.domain.Rol;
import cancha.domain.RolUsuario;
import cancha.domain.Usuario;

@ViewController
@PreviousView("/rolUsuario_list.xhtml")
public class RolUsuarioEditMB extends AbstractEditPageBean<RolUsuario, Integer> {

	private static final long serialVersionUID = 1L;

	@Inject
	private RolUsuarioBC usuarioRolBC;

	@Inject
	private UsuarioBC usuarioBC;

	@Inject
	private RolBC rolBC;

	private Usuario usuarioSeleccionado = new Usuario();
	
	private Rol rolSeleccionado = new Rol();
	
	private List<Usuario> listaUsuarios;
	
	private List<Rol> listaRols;
	
	private List<Rol> rolsSeleccionados = new ArrayList<Rol>();
	
	private List<Rol> tieneRols;
	private List<Rol> noTieneRols;
	
	private LazyDataModel<Rol> rolModel;	

	private HashMap<Usuario, Boolean> usuariosMap = new HashMap<Usuario, Boolean>();
	private HashMap<Integer, Boolean> rolsMap = new HashMap<Integer, Boolean>();
	//Se obtiene el archivo de mensajes
	FacesContext ctx = FacesContext.getCurrentInstance();
	Locale locale = ctx.getViewRoot().getLocale();
	ResourceBundle mensajes = (ResourceBundle) ResourceBundle.getBundle("messages", locale);
	
	/**
	 * Carga la lista de usuario
	 */
	@SuppressWarnings("serial")
	@PostConstruct
	public void loadLazyModel() {
		listaUsuarios = usuarioBC.findAll();
		setListaRols(rolBC.findAll());
		
		rolModel = new LazyDataModel<Rol>() {
			
			@Override
			public List<Rol> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
					
					if(sortField == null) sortField = "id"; //default sort field
					
					List<Rol> roles = new ArrayList<Rol>();
					roles = rolBC.findPage(pageSize, first, sortField, sortOrder.equals(SortOrder.ASCENDING)? true: false);
					
					//Aplicar filtro sobre el listado
					List<Rol> filtrados = new ArrayList<Rol>();
					if(filters != null){
						for(Rol rol : roles) {
				            boolean match = true;
				 
				            if (filters != null) {
				                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
				                    try {
				                        String filterProperty = it.next();
				                        Object filterValue = filters.get(filterProperty);
				                        Field field = rol.getClass().getDeclaredField(filterProperty);
				                        field.setAccessible(true);
				                        String fieldValue = String.valueOf(field.get(rol));
				                        if(filterValue == null || fieldValue.toLowerCase().contains(filterValue.toString().toLowerCase())) {
				                            match = true;
				                    }
				                    else {
				                            match = false;
				                            break;
				                        }
				                    } catch(Exception e) {
				                        match = false;
				                    }
				                }
				            }
				 
				            if(match) {
				                filtrados.add(rol);
				            }
				        }
						return filtrados;
					}
					return roles;
			}
			
		};
	
		rolModel.setRowCount(rolBC.count());
		rolModel.setPageSize(10);
		
	}
	
	public LazyDataModel<Rol> getRolModel() {
	    return rolModel;
	}
	
	
	
	/**
	 * Borrado logico
	 */
	@Override
	@Transactional
	public String delete() {
		this.usuarioRolBC.delete(getId());
		//this.usuarioRolBC.update(getBean());
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, mensajes.getString("eliminado.exito"), null);  
        FacesContext.getCurrentInstance().addMessage("suceso", msg); 
		return getPreviousView();
	}

	/**
	 * Inserta un nuevo usuarioRol
	 */
	@Override
	@Transactional
	public String insert() {
		this.usuarioSeleccionado = usuarioBC.load(this.usuarioSeleccionado.getId());
		this.rolSeleccionado = rolBC.load(this.rolSeleccionado.getId());
		RolUsuario rolUsuario = new RolUsuario();
		rolUsuario.setUsuario(usuarioSeleccionado);
		rolUsuario.setRol(rolSeleccionado);
		
		for(RolUsuario rolU: usuarioSeleccionado.getRolUsuarios()){
			usuarioRolBC.delete(rolU.getId());
		}
		try{
			usuarioRolBC.insert(rolUsuario);

		}catch(javax.persistence.PersistenceException ex){//Si existe el el rol
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, mensajes.getString("usuarioRol.existe"), null);  
	        FacesContext.getCurrentInstance().addMessage("suceso", msg);
	        return getNextView();
		}
		
		rolsMap = new HashMap<Integer, Boolean>();
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, mensajes.getString("agregado.exito"), null);  
        FacesContext.getCurrentInstance().addMessage("suceso", msg); 
		return getNextView();
	}

	/**
	 * Actualiza un usuarioRol
	 */
	@Override
	@Transactional
	public String update() {
		this.usuarioRolBC.update(getBean());
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, mensajes.getString("modificado.exito"), null);  
        FacesContext.getCurrentInstance().addMessage("suceso", msg); 
		return getPreviousView();
	}

	@Override
	protected void handleLoad() {
		setBean(this.usuarioRolBC.load(getId()));
	}

	/**
	 * Actualiza el listado de rols una vez que se seleccione un usuario
	 */
	public void actualizarRols() {
		Usuario usuario = usuarioBC.load(usuarioSeleccionado.getId());
		for (Iterator<Integer> iter = getRolsMap().keySet().iterator(); iter.hasNext();) {
			iter.next();
			iter.remove();
		}
		Rol rol;
		rolsMap = new HashMap<Integer, Boolean>();
		/*for(RolUsuario usuarioRol : usuario.getRolUsuarios()){
			rol = usuarioRol.getRol();
			rolsMap.put(rol.getId(), Boolean.TRUE);
		}*/
		
	}

	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}

	public List<Rol> getRolsSeleccionados() {
		return rolsSeleccionados;
	}

	public void setRolsSeleccionados(List<Rol> rolsSeleccionados) {
		this.rolsSeleccionados = rolsSeleccionados;
	}


	public HashMap<Usuario, Boolean> getUsuariosMap() {
		return usuariosMap;
	}


	public void setUsuariosMap(HashMap<Usuario, Boolean> usuariosMap) {
		this.usuariosMap = usuariosMap;
	}


	public HashMap<Integer, Boolean> getRolsMap() {
		return rolsMap;
	}


	public void setRolsMap(HashMap<Integer, Boolean> rolsMap) {
		this.rolsMap = rolsMap;
	}


	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}


	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<Rol> getTieneRols() {
		return tieneRols;
	}


	public void setTieneRols(List<Rol> tieneRols) {
		this.tieneRols = tieneRols;
	}


	public List<Rol> getNoTieneRols() {
		return noTieneRols;
	}


	public void setNoTieneRols(List<Rol> noTieneRols) {
		this.noTieneRols = noTieneRols;
	}

	public Rol getRolSeleccionado() {
		return rolSeleccionado;
	}

	public void setRolSeleccionado(Rol rolSeleccionado) {
		this.rolSeleccionado = rolSeleccionado;
	}

	public List<Rol> getListaRols() {
		return listaRols;
	}

	public void setListaRols(List<Rol> listaRols) {
		this.listaRols = listaRols;
	}

}
