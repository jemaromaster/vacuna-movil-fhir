/*
 * TICPY Framework
 * Copyright (C) 2012 Plan Director TICs
 *
----------------------------------------------------------------------------
 * Originally developed by SERPRO
 * Demoiselle Framework
 * Copyright (C) 2010 SERPRO
 *
----------------------------------------------------------------------------
 * This file is part of TICPY Framework.
 *
 * TICPY Framework is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License version 3
 * along with this program; if not,  see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301, USA.
 *
----------------------------------------------------------------------------
 * Este archivo es parte del Framework TICPY.
 *
 * El TICPY Framework es software libre; Usted puede redistribuirlo y/o
 * modificarlo bajo los términos de la GNU Lesser General Public Licence versión 3
 * de la Free Software Foundation.
 *
 * Este programa es distribuido con la esperanza que sea de utilidad,
 * pero sin NINGUNA GARANTÍA; sin una garantía implícita de ADECUACION a cualquier
 * MERCADO o APLICACION EN PARTICULAR. vea la GNU General Public Licence
 * más detalles.
 *
 * Usted deberá haber recibido una copia de la GNU Lesser General Public Licence versión 3
 * "LICENCA.txt", junto con este programa; en caso que no, acceda a <http://www.gnu.org/licenses/>
 * o escriba a la Free Software Foundation (FSF) Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02111-1301, USA.
 */
package cancha.view;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.ticpy.tekoporu.annotation.PreviousView;
import org.ticpy.tekoporu.stereotype.ViewController;
import org.ticpy.tekoporu.template.AbstractEditPageBean;
import org.ticpy.tekoporu.transaction.Transactional;

import ca.uhn.fhir.model.dstu.resource.Immunization;
import ca.uhn.fhir.model.dstu.resource.Patient;
import cancha.domain.Vacuna;
import cancha.util.MapperFHIRDomain;
import cancha.util.RestClient;
import cancha.util.UsuarioUtils;

@ViewController
@PreviousView("/vacuna_list.xhtml")
public class VacunaEditMB extends AbstractEditPageBean<Vacuna, Integer> {

	private static final long serialVersionUID = 1L;

	private Boolean rechazoVacuna = false;

	Patient pacienteEnCuestion;
	String pacienteCedula;

	@Inject
	RestClient restClient;

	@Inject
	UsuarioUtils usuarioUtils;

	public String guardarVacuna() {
		return getPreviousView();
	}

	@Override
	@Transactional
	public String delete() {
		// this.vacunaBC.delete(getId());
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Vacuna eliminado exitosamente", null);
		FacesContext.getCurrentInstance().addMessage("suceso", msg);
		return getPreviousView();
	}

	@Override
	@Transactional
	public String insert() {
		Vacuna v = getBean();

		MapperFHIRDomain mapper = new MapperFHIRDomain();
		Immunization immu = mapper.map(v, pacienteEnCuestion);
		restClient.postVacuna(immu);
		// } catch (Exception ex) {
		// FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
		// "Error al guardar", null);
		// FacesContext.getCurrentInstance().addMessage("suceso", msg);
		// return getPreviousView();
		// }

		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Vacuna agregado exitosamente", null);
		FacesContext.getCurrentInstance().addMessage("suceso", msg);
		return getPreviousView();
	}

	@Override
	@Transactional
	public String update() {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Vacuna actualizado exitosamente", null);
		FacesContext.getCurrentInstance().addMessage("suceso", msg);
		return getPreviousView();
	}

	@PostConstruct
	public void init() {
		// setListaRols(rolBC.findAll());
		// if(getBean().getRolVacunas() != null &&
		// !getBean().getRolVacunas().isEmpty()){
		// this.rolSeleccionado = getBean().getRolVacunas().get(0).getRol();
		// }
		getBean().setBooleanRechazo(false);

		pacienteEnCuestion = (Patient) usuarioUtils.obtenerDeSesion("paciente");
		pacienteCedula = (String) usuarioUtils
				.obtenerDeSesion("pacienteCedula");

		try {
			if (pacienteEnCuestion == null) {
				ExternalContext context = FacesContext.getCurrentInstance()
						.getExternalContext();

				context.redirect(context.getRequestContextPath()
						+ "/index.xhtml");

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void handleLoad() {
		// setBean(this.vacunaBC.load(getId()));
	}

	public Boolean getRechazoVacuna() {
		return rechazoVacuna;
	}

	public void setRechazoVacuna(Boolean rechazoVacuna) {
		this.rechazoVacuna = rechazoVacuna;
	}

}
