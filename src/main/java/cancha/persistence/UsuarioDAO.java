package cancha.persistence;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.slf4j.Logger;
import org.ticpy.tekoporu.template.JPACrud;

import cancha.domain.Usuario;

public class UsuarioDAO extends JPACrud<Usuario, Integer>{

private static final long serialVersionUID = 1L;
	
	@Inject
	@SuppressWarnings("unused")
	private Logger logger;
	
	@Inject
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<Usuario> findPage(int pageSize, int first, String sortField, boolean sortOrderAsc) {
		
		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(Usuario.class);
		
		//add order by
		Order order = Order.asc(sortField);
		if(!sortOrderAsc) 
			order = Order.desc(sortField);
		criteria.addOrder(order);

		//add limit, offset
		criteria.setFirstResult(first);
		criteria.setMaxResults(pageSize);
		
		return criteria.list();

	}
	
	public int count() {
		Query q = em.createQuery("select count(*) from Usuario r");
		return ((Long) q.getSingleResult()).intValue();
		
	}
	
	public Usuario findUser(String username){
		
		Query q = em.createQuery("select u from Usuario u where u.username = :username");
		q.setParameter("username", username);
		return ((Usuario) q.getSingleResult());
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> find() {
		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(Usuario.class);
		
		Order order = Order.asc("id");
		criteria.addOrder(order);
		
		return criteria.list();

	}
	
}

