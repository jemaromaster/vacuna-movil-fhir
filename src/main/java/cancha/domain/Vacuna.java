package cancha.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the vacuna database table.
 * 
 */
public class Vacuna implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String tipoVacuna; 
	
	private String fabricante; 
	
	private String hospitalNosocomio; 
	
	private String loteNumero;
	
	private Date fechaVencimiento;
	
	private String siteBrazo;
	
	private String viaRoute;
	
	private BigDecimal cantidadML;
	
	private Date fecha;
	private Boolean booleanRechazo;
	public Boolean getBooleanRechazo() {
		return booleanRechazo;
	}


	public void setBooleanRechazo(Boolean booleanRechazo) {
		this.booleanRechazo = booleanRechazo;
	}


	public String getObservacionRechazo() {
		return observacionRechazo;
	}


	public void setObservacionRechazo(String observacionRechazo) {
		this.observacionRechazo = observacionRechazo;
	}


	private String observacionRechazo;

	
	public String getTipoVacuna() {
		return tipoVacuna;
	}


	public void setTipoVacuna(String tipoVacuna) {
		this.tipoVacuna = tipoVacuna;
	}


	public String getFabricante() {
		return fabricante;
	}


	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}


	public String getHospitalNosocomio() {
		return hospitalNosocomio;
	}


	public void setHospitalNosocomio(String hospitalNosocomio) {
		this.hospitalNosocomio = hospitalNosocomio;
	}


	public String getLoteNumero() {
		return loteNumero;
	}


	public void setLoteNumero(String loteNumero) {
		this.loteNumero = loteNumero;
	}


	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}


	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}


	public String getSiteBrazo() {
		return siteBrazo;
	}


	public void setSiteBrazo(String siteBrazo) {
		this.siteBrazo = siteBrazo;
	}


	public String getViaRoute() {
		return viaRoute;
	}


	public void setViaRoute(String viaRoute) {
		this.viaRoute = viaRoute;
	}


	public BigDecimal getCantidadML() {
		return cantidadML;
	}


	public void setCantidadML(BigDecimal cantidadML) {
		this.cantidadML = cantidadML;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	

	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public boolean equals(Object object) {

		if (!(object instanceof Vacuna)) {
			return false;
		}

		Vacuna other = (Vacuna) object;

		if (this.getId() != null && this.getId().equals(other.getId())) {
			return true;
		}
		return false;
	}
}